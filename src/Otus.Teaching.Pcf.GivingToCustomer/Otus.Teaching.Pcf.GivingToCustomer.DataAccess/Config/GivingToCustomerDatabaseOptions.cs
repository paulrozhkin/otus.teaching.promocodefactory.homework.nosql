﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Config
{
    public class GivingToCustomerDatabaseOptions
    {
        public const string GivingToCustomerDatabase = nameof(GivingToCustomerDatabase);

        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;
    }
}
