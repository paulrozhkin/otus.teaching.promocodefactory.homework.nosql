﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public static class FakeDataFactory
    {
        
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static List<PromoCode> Promocodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("de5fbbd2-fc37-4b45-84f4-e51b53492fe4"),
                BeginDate = new DateTime(2021, 12, 17, 0, 0, 0),
                Code = "BLACK FRIDAY",
                EndDate = new DateTime(2021, 12, 17, 23, 59, 59),
                PartnerId = Guid.Parse("20d2d612-db93-4ed5-86b1-ff2413bca655"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                ServiceInfo = "WE SELL EVERYTHING"
            }
        };

        public static List<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<Preference>()
                        {
                            Preferences.First(x => x.Id == Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")),
                            Preferences.First(x => x.Id == Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"))
                        },
                        PromoCodes = new List<PromoCode>()
                        {
                            Promocodes.First(x => x.Id == Guid.Parse("de5fbbd2-fc37-4b45-84f4-e51b53492fe4"))
                        }
                    }
                };

                return customers;
            }
        }
    }
}