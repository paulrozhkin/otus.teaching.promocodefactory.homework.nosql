﻿using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly MongoDataContext _mongoDataContext;

        static MongoDbInitializer()
        {
            ConfigureEntities();
        }

        public MongoDbInitializer(MongoDataContext mongoDataContext)
        {
            _mongoDataContext = mongoDataContext;
        }
        
        public void InitializeDb()
        {
            _mongoDataContext.DropDatabase();
            _mongoDataContext.CreateDatabase();

            _mongoDataContext.AddRange(FakeDataFactory.Preferences, MongoDataContext.PreferencesCollection);
            _mongoDataContext.AddRange(FakeDataFactory.Promocodes, MongoDataContext.PromocodesCollection);
            _mongoDataContext.AddRange(FakeDataFactory.Customers, MongoDataContext.CustomersCollection);
        }

        private static void ConfigureEntities()
        {
            BsonClassMap.RegisterClassMap<BaseEntity>(cm =>
            {
                cm.SetIsRootClass(true);
                cm.AutoMap();
                cm.MapIdMember(c => c.Id);
            });

            BsonClassMap.RegisterClassMap<Customer>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.UnmapMember(m => m.FullName);
            });

        }
    }
}