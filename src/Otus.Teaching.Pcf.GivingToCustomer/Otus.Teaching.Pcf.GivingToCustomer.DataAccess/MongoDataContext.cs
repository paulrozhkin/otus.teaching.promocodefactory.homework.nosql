﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Config;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDataContext
    {
        public const string PreferencesCollection = "Preferences";

        public const string CustomersCollection = "Customers";

        public const string PromocodesCollection = "Promocodes";


        private readonly MongoClient _mongoClient;
        private readonly string _databaseName;
        public IMongoDatabase MongoDatabase { get; set; }

        public MongoDataContext(IOptions<GivingToCustomerDatabaseOptions> bookStoreDatabaseSettings)
        {
            _databaseName = bookStoreDatabaseSettings.Value.DatabaseName;

            _mongoClient = new MongoClient(
                bookStoreDatabaseSettings.Value.ConnectionString);
        }

        public void DropDatabase()
        {
            _mongoClient.DropDatabase(_databaseName);
        }

        public void CreateDatabase()
        {
            MongoDatabase = _mongoClient.GetDatabase(_databaseName);

        }

        public void AddRange<T>(IEnumerable<T> entities, string collectionName)
        {
            _ = MongoDatabase ?? throw new InvalidOperationException("Database not created");

            var collection = MongoDatabase.GetCollection<T>(collectionName);
            collection.InsertMany(entities);
        }
    }
}