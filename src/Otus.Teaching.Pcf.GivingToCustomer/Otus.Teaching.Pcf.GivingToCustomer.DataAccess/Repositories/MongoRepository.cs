﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly IMongoCollection<T> _mongoCollection;

        public MongoRepository(MongoDataContext dataContext, string collectionName)
        {
            _mongoCollection = dataContext.MongoDatabase.GetCollection<T>(collectionName);
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await _mongoCollection.Find(x => true).ToListAsync();
            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _mongoCollection.Find(x => x.Id == id).FirstOrDefaultAsync();
            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _mongoCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var entities = await _mongoCollection.Find(predicate).ToListAsync();
            return entities;
        }

        public async Task AddAsync(T entity)
        {
            await _mongoCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _mongoCollection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }

        public async Task UpdateAsync(IEnumerable<T> entities)
        {
            var updates = new List<WriteModel<T>>();

            foreach (var doc in entities)
            {
                var filter = Builders<T>.Filter.Eq(nameof(doc.Id), doc.Id);
                updates.Add(new ReplaceOneModel<T>(filter, doc));
            }
            await _mongoCollection.BulkWriteAsync(updates, new BulkWriteOptions() { IsOrdered = false });
        }

        public async Task DeleteAsync(T entity)
        {
            await _mongoCollection.DeleteOneAsync(x => x.Id == entity.Id);
        }
    }
}