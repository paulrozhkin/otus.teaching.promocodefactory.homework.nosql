﻿using System;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Config;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class MongoDatabaseFixture : IDisposable
    {
        public MongoDatabaseFixture()
        {
            var settings = new GivingToCustomerDatabaseOptions()
            {
                ConnectionString = "mongodb://root:password@localhost:27017",
                DatabaseName = "GivingToCustomerStore"
            };
            var options = Options.Create<GivingToCustomerDatabaseOptions>(settings);

            DbContext = new MongoDataContext(options);

            var mongoTestDbInitializer = new MongoDbInitializer(DbContext);
            mongoTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            DbContext.DropDatabase();
        }

        public MongoDataContext DbContext { get; private set; }
    }
}